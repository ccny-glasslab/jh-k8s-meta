# jh-k8s-meta

[<img src="https://img.shields.io/badge/slack-glasslab-yellow.svg?logo=slack">][slack]
[<img src="https://img.shields.io/badge/trello-jh--k8s--board-orange.svg?logo=trello">][trello]

### Project management links
* [Google Drive folder][drive] - contains meeting notes and shared files

### Scripts
1. [Step-by-step description of Nginx + SSL setup][walk-through-nginx-ssl]
    * [Wrote an explanation about the purpose of setup here][walk-through-nginx-ssl-explanation]
    * Used Let's Encrypt, Certbot for SSL
    * Used Freenom for free domain, free DNS records
    * Steps work on a Arch Linux host machine and a CentOS guest VM
1. [Install/configure script for CCNY Hub (2nd generation)][ccnyhub-gen-2-repo]

### Misc links
* [Domain-1][domain-1] - was used with TLJH
* [Domain-2][domain-2] - was used with nginx

[slack]: https://glasslab.slack.com/
[trello]: https://trello.com/b/3lTkUnxg/jh-k8s
[drive]: https://drive.google.com/drive/folders/1CT6SEmkS8EoxMwFWT-XxFdYw-ordOYAW
[walk-through-nginx-ssl]: https://gist.github.com/ian-s-mcb/a352bb0a1743decce5ff3e7755af91bc
[walk-through-nginx-ssl-explanation]: https://docs.google.com/document/d/1gLsBl7ypY3Qmwj6yaPFPv-yftSYTdvMFHZKwaX1GY18/edit?usp=sharing
[ccnyhub-gen-2-repo]: https://gitlab.com/ccny-glasslab/ccnyhub-gen-2
[domain-1]: https://ccnyhub.org
[domain-2]: https://ccnyhub.com/
